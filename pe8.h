#ifndef PE8_H
#define PE8_H

#include <stdint.h>
#include <string>

/**
 * helper function to convert an ASCII char to an int
 * @param value source convert value
 * @tparam INT_T data type to cast and return as
 */
template<typename INT_T>
INT_T char_to_int(const char &value)
{
  return (static_cast<INT_T>(value) - 48);
}

/**
 * Find greatest product of 4 and 8 digits any input
 * @param input_number provide a large number as a string to search in
 * @returns greatest product of 4 and 8 digits any input
 */
std::string find_greatest_product(const std::string& input_number);

/**
 * Find greatest product any number of digits any input
 * @param input_number provide a large number as a string to search in
 * @param num_digits number of adjacent digits to search
 * @returns greatest product of num_digits
 */
std::string find_greatest_product(uint32_t num_digits, const std::string& input_number);

#endif // PE8_H
