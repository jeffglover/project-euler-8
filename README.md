# Project Euler 8
## Dependencies
Tested with the following libraries on Arch Linux as of September 1, 2016
* CMake 3.6.1
* GCC 6.1.1 20160802

## Docker
My Arch Linux Docker image is publicly available on [DockerHub](https://hub.docker.com/r/jeffglover/archlinux/tags/)
```bash
docker run -t -i --rm jeffglover/archlinux:build-2016-09-01 /bin/bash
```

## Building
```bash
mkdir build
cd build
cmake <PATH TO SOURCE>
make
# run unit tests
make test
```

## Usage
INPUT STRING: defaults to the string provided in the requirements
NUM_DIGITS: defaults to printing both 4 and 8 adjacent digits
```bash
./project-euler-8 <INPUT STRING> <NUM DIGITS>
```
