#include <stdlib.h>
#include <iostream>
#include <sstream>

#include "pe8.h"

std::string find_greatest_product(const std::string& input_number)
{
  std::ostringstream result;
  result << find_greatest_product(4, input_number) << std::endl;
  result << find_greatest_product(8, input_number);
  return (result.str());
}

std::string find_greatest_product(uint32_t num_digits, const std::string& input_number)
{
  if (input_number.length() < num_digits)
  {
    return("input number is < num_digits, cannot find product");
  }
  
  uint64_t product = 1;
  uint64_t greatest_product = 0;
  
  /* will store the digits used to create the greatest product */
  std::string greatest_result("");
  
  /* moving window of num_digits */
  for (size_t window=0; window < input_number.length() - (num_digits-1); ++window)
  {
    std::ostringstream result;
    
    /* start with the first digit */
    uint64_t digit = char_to_int<uint64_t>(input_number[window]);
    result << input_number[window];
    product = digit;
    
    /* find the product of the digits in the window */
    for (size_t digit_index=window+1; digit_index < window+num_digits; ++digit_index)
    {
      digit = char_to_int<uint64_t>(input_number[digit_index]);
      result << " x " << input_number[digit_index];
      product = product * digit;
    }
    
    /* keep track of the greatest product */
    if (greatest_product <= product)
    {
      greatest_product = product;
      greatest_result = result.str();
    }
  }
  std::ostringstream result;
  result << greatest_result << " = " << greatest_product;
  return (result.str());
}
